<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    $devices = $this->db->query("SELECT * FROM device 
                                JOIN device_type ON device.id_device_type = device_type.id_device_type 
                                ORDER BY id LIMIT 3");
    
    $tplVars['devices'] = $devices->fetchAll();
    


    return $this->view->render($response, 'index.latte', $tplVars);
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

$app->get('/devices', function( Request $request, Response $response, $args){
    $devices = $this->db->query("SELECT * FROM device ORDER BY id");
    
    $tplVars['devices'] = $devices->fetchAll();
    


    return $this->view->render($response, 'list.latte', $tplVars);
})->setName('listDevices');

$app->get('/schedules', function( Request $request, Response $response, $args){
    $devices = $this->db->query("SELECT * FROM schedule ORDER BY id_schedule");
    
    $tplVars['schedules'] = $devices->fetchAll();
    


    return $this->view->render($response, 'list-schedules.latte', $tplVars);
})->setName('listSchedules');


$app->post('/schedules', function( Request $request, Response $response, $args){
    $data = $request->getParsedBody();
    if (empty($data['start']) || empty($data['stop']) || !isset($data['state'])){
        $tplVars['alert'] = 'Missing informations'. $data['state'] . $data['start'] . $data['stop'];
    } else {
        try {
            $stmt = $this->db->prepare(
                "INSERT INTO schedule(start, stop, state) VALUES (:start, :stop, :state)"
            );

            $stmt->bindValue(':start', $data['start']);
            $stmt->bindValue(':stop', $data['stop']);
            $stmt->bindValue(':state', $data['state']);

            $stmt->execute();



        } catch(PDOException $e) {
            $this->logger->error($e->getMessage());
            print($data['device_name']);
            exit("Cannot add device" . $e->getMessage());
        }

    }
    


    return $response->withRedirect($this->router->pathFor('listSchedules'));

})->setName('newSchedule');

$app->post('/delete-schedule', function( Request $request, Response $response, $args){
    $schedule = $request->getParsedBody();
    if (empty($schedule['id'])) {

        $tplVars['alert'] = 'Missing schedule id';
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM schedule WHERE id_schedule = :id");
            $stmt->bindValue(':id', $schedule['id']);
            $stmt->execute();
        } catch(PDOException $e) {
            $this->logger->error($e->getMessage());
            exit("Cannot delete schedule" . $e->getMessage());
        }
    }
    return $response->withRedirect($this->router->pathFor('listSchedules'));

})->setName('deleteSchedule');


$app->get('/new-device', function( Request $request, Response $response, $args){
    $all_ports = array(1, 2, 3, 4, 5, 6);
    $used_ports = $this->db->query("SELECT id_port FROM device");
    $used_ports1 = $used_ports->fetchAll(PDO::FETCH_COLUMN, 0);
    $available_ports = array_diff($all_ports,$used_ports1);
    $tplVars['available_ports'] = $available_ports;
    $types = $this->db->query("SELECT * FROM device_type");
    $tplVars['types'] = $types->fetchAll();
    $schedules = $this->db->query("SELECT * FROM schedule");
    $tplVars['schedules'] = $schedules->fetchAll();

    return $this->view->render($response, 'new-device.latte', $tplVars);
})->setName('newDevicePage');

$app->post('/delete-device', function(Request $request, Response $response, $args){
    $device_id = $request->getParsedBody();
    if (empty($device_id['id'])) {

        $tplVars['alert'] = 'Missing device id' . $device_id['id'];
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM device WHERE id = :id");
            $stmt->bindValue(':id', $device_id['id']);
            $stmt->execute();
        } catch(PDOException $e) {
            $this->logger->error($e->getMessage());
            exit("Cannot delete device" . $e->getMessage());
        }
    }
    return $response->withRedirect($this->router->pathFor('listDevices'));
})->setName('deleteDevice');

$app->post('/new', function( Request $request, Response $response, $args){
    $data = $request->getParsedBody();
    if (empty('device_name') || empty('port')){
        $tplVars['alert'] = 'Missing device name and port';
    } else {
        try {
            $stmt = $this->db->prepare(
                "INSERT INTO device(name, id_device_type, id_port, id_schedule, state) VALUES (:name, :id_device_type, :id_port, :id_schedule, '0')"
            );

            $stmt->bindValue(':name', $data['device_name']);
            $stmt->bindValue(':id_device_type', $data['type']);
            $stmt->bindValue(':id_port', $data['port']);
            $stmt->bindValue(':id_schedule', $data['id_schedule']);

            $stmt->execute();



        } catch(PDOException $e) {
            $this->logger->error($e->getMessage());
            print($data['device_name']);
            exit("Cannot add device" . $e->getMessage());
        }

    }


    return $response->withRedirect($this->router->pathFor('listDevices'));
})->setName('newDevice');



$app->get('/api/devices', function( Request $request, Response $response, $args){
    $stmt = $this->db->query("SELECT * FROM device");
    $devices = $stmt->fetchAll();
    $response_data['devices'] = $devices;
    $response->write(json_encode($response_data));

    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);
});

$app->put('/api/{port}/{state}', function( Request $request, Response $response, $args){
    $port = $request->getAttribute('port');
    $state = $request->getAttribute('state');
    if ((1 <= $port) && ($port <= 6)) {
        if ($state == 'on') {
            $stmt = $this->db->query("UPDATE device SET state = true WHERE id_port=$port");
        } elseif ($state == 'off') {
            $stmt = $this->db->query("UPDATE device SET state = false WHERE id_port=$port");
        } else {
            $response->write('Bad state');
        }
    } else {
        $response->write('Bad port');
        return $response
        ->withHeader('Content-type', 'text')
        ->withStatus(404);
    }   

    return $response
        ->withHeader('Content-type', 'text')
        ->withStatus(200);
});

$app->get('/api/schedules', function( Request $request, Response $response, $args){
    $stmt = $this->db->query("SELECT * FROM schedule");
    $schedules = $stmt->fetchAll();
    $response_data['schedules'] = $schedules;
    $response->write(json_encode($response_data));

    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);
});

$app->put('/api/schedules/{id}/{state}', function( Request $request, Response $response, $args){
    $id = $request->getAttribute('id');
    $state = $request->getAttribute('state');
    $count = $this->db->query('SELECT COUNT(*) FROM schedule');
    if ((1 <= $id) && ($port <= $count)) {
        if ($state == 'on') {
            $stmt = $this->db->query("UPDATE schedule SET state = true WHERE id_schedule=$id");
        } elseif ($state == 'off') {
            $stmt = $this->db->query("UPDATE schedule SET state = false WHERE id_schedule=$id");
        } else {
            $response->write('Bad state');
        }
    } else {
        $response->write('Bad id');
        return $response
        ->withHeader('Content-type', 'text')
        ->withStatus(404);
    }   

    return $response
        ->withHeader('Content-type', 'text')
        ->withStatus(200);
});


