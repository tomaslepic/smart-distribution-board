let request = new XMLHttpRequest()
let baseUrl = new URL('http://localhost:8080/api/')
const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }
  sleep(500).then(() => {
    //do stuff
    console.log($('input:checkbox'));
    
    $('input:checkbox').each(function(index, element){
        $(this).change(function() {
            var portId = $(this).attr( "data-port-id" )  
            console.log(portId)
            if ($(this).prop('checked')) {
                let url = baseUrl + portId + '/on'
                request.open('PUT', url, true)
                request.send()
                console.log("Checked")
            } else {
                let url = baseUrl + portId + '/off'
                request.open('PUT', url, true)
                request.send()
                console.log("Unchecked")
            }

            request.onreadystatechange=(e)=>{
                console.log(request.responseText)
            }
            })

    });
    
  })

