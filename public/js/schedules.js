let request = new XMLHttpRequest()
let baseUrl = new URL('http://localhost:8080/api/')
const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }
  sleep(500).then(() => {
    //do stuff
    console.log($('input:checkbox'));
    
    $('input:checkbox').each(function(index, element){
        $(this).change(function() {
            var scheduleId = $(this).attr( "data-schedule-id" )  
            console.log(scheduleId)
            if ($(this).prop('checked')) {
                let url = baseUrl + 'schedules/' + scheduleId + '/on'
                request.open('PUT', url, true)
                request.send()
                console.log("Checked")
            } else {
                let url = baseUrl + 'schedules/' + scheduleId + '/off'
                request.open('PUT', url, true)
                request.send()
                console.log("Unchecked")
            }

            request.onreadystatechange=(e)=>{
                console.log(request.responseText)
            }
            })

    });
    
  })

